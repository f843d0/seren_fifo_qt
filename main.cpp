#include "mainWindow.h"
#include <cstdlib>
#include <sstream>

void MainWindow::handleButton()
{
    for (auto && it : buttonsToFiles)
    {
        if (it.second.compare(((QPushButton *)sender())->text().toUtf8().constData()) == 0)
        {
            std::ostringstream oss;
            oss << "ffmpeg -i res/"<< it.first << " -ac 2 -ar 48000 -f s16le pipe:1 > ~/.seren/fifo_in";
            system(oss.str().c_str());
        }
    }
}

void MainWindow::fillMapFromConfigFile()
{
    json_object *root = json_object_from_file("res/conf/seren_fifo.conf");
    json_object *mapping = json_object_new_array();
    if (json_object_object_get_ex(root, "mapping", &mapping))
    {
        for (size_t i = 0; i < json_object_array_length(mapping); ++i)
        {
            json_object *config_entry = json_object_array_get_idx(mapping, i);
            if (!config_entry)
            {
                continue;
            }
            struct lh_entry *entry = json_object_get_object(config_entry)->head;
            if (!entry)
            {
                json_object_put(config_entry);
                continue;
            }
            json_object *file_label = NULL;
            if (json_object_object_get_ex(config_entry, (const char *)entry->k, &file_label))
            {
                struct lh_entry *entry_file = json_object_get_object(file_label)->head;
                json_object *label = NULL;
                if (json_object_object_get_ex(file_label, (const char *)entry_file->k, &label))
                {
                    //std::cout << (const char *)entry->k << " : " << (const char *)entry_file->k << " : " << json_object_get_string(label) << std::endl;
                    mapper[(const char *)entry->k].insert(std::pair<std::string, std::string>((const char *)entry_file->k, json_object_get_string(label)));
                }
            }
        }
    }
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    mainGroup = std::make_shared<QGroupBox>();
    mainGroupLayout = std::make_shared<QHBoxLayout>();

    fillMapFromConfigFile();

    for (auto && it : mapper)
    {
        categories.push_back(std::make_shared<QGroupBox>(QObject::tr(it.first.c_str())));
        rows.push_back(std::make_shared<QVBoxLayout>());
        scrollers.push_back(std::make_shared<QScrollArea>());
        for (auto && iit : it.second)
        {
            //std::cout << it.first << ":" << iit.first << ":" << iit.second << std::endl;
            buttonsToFiles[iit.first] = iit.second;
            buttons.push_back(std::make_shared<QPushButton>(QObject::tr(iit.second.c_str())));
            rows.back()->addWidget(buttons.back().get());
            QObject::connect(buttons.back().get(), SIGNAL(released()), this, SLOT(handleButton()));
        }
        //auto cat = std::find_if(categories.begin(), categories.end(), [&](const auto &val){return !val->title().toUtf8().toStdString().compare(it.first);});
        categories.back()->setLayout(rows.back().get());
        scrollers.back()->setWidget(categories.back().get());
        scrollers.back()->setWidgetResizable(true);
        mainGroupLayout->addWidget(scrollers.back().get());
    }
    mainGroup->setLayout(mainGroupLayout.get());
    mainGroup->show();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    MainWindow mainWindow;
    return app.exec();
}
