#ifndef __MAIN_WINDOW_H__
#define __MAIN_WINDOW_H__

#include <QApplication>
#include <QPushButton>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMainWindow>
#include <QScrollArea>
#include <vector>
#include <memory>
#include <map>

#include <json.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow() {}
    void fillMapFromConfigFile();

public slots:
    void handleButton();
private:
    std::vector<std::shared_ptr<QPushButton> > buttons;
    std::vector<std::shared_ptr<QGroupBox> > categories;
    std::vector<std::shared_ptr<QScrollArea> > scrollers;
    std::vector<std::shared_ptr<QVBoxLayout> > rows;
    std::shared_ptr<QHBoxLayout> mainGroupLayout;
    std::shared_ptr<QGroupBox> mainGroup;
	std::map <std::string, std::string> buttonsToFiles;
    std::map <std::string, std::map<std::string, std::string> > mapper;
};

#endif
